extends RigidBody

func _ready():
	contact_monitor = true

func hit(body):
	if body.has_method("hitted"):
		$HitSFX.play()
		$Autodestroy.start()
		body.hitted()
		visible = false


func _on_Autodestroy_timeout():
	queue_free()
