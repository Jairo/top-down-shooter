extends ParallaxBackground

export (float) var speed

func _process(delta):
	offset.y += speed * delta
	
	set_scroll_offset(Vector2(0, offset.y))
