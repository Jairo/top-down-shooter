extends Node

func getScreenHalfWidth():
	return get_viewport().get_camera().size * get_viewport().size.x / get_viewport().size.y / 2

func getScreenHalfHeight():
	return get_viewport().get_camera().size / 2
